#!/bin/bash

docker swarm leave --force
yes | docker volume prune
cd nginx-php-fpm/
docker build -t basewebtest .
cd ..
cd craftstack/
docker build -t webtest .

docker swarm init
docker stack deploy -c stack-docker.yml stack
